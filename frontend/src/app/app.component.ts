import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  letters = "abcdefghijklmnopqrstuvwxyz1234567890".split("");

  gameId = "";
  word = '';
  gameState = "ONGOING";
  guessesLeft = 0;
  name = "";
  highscores = [];

  constructor(private http: HttpClient, private modalService: NgbModal) {}

  ngOnInit() {
    this.startGame();
  }

  processResponse(response: GuessResponse): void {
    this.gameId = response.gameId;
    this.word = response.visibleWord;
    this.gameState = response.gameState;
    this.guessesLeft = response.guessesLeft;
  }

  startGame() : void {
    this.http
    .post<GuessResponse>("http://127.0.0.1:5000/start_game", {})
    .subscribe( response => {
      this.processResponse(response);
    });
    this.getHighscores();
  }

  guessLetter(letter: String): void {
    this.http
      .post<GuessResponse>("http://127.0.0.1:5000/guess_letter", {gameId: this.gameId, guess: letter})
      .subscribe( response => {
        this.processResponse(response);
      });
  }

  saveScore(): void {
    this.http
      .post("http://127.0.0.1:5000/save_score", {gameId: this.gameId, name: this.name})
      .subscribe (response => {
        this.startGame();
      });
  }

  getHighscores(): void {
    this.http
      .get<[ScoreEntry]>("http://127.0.0.1:5000/get_highscores")
      .subscribe( response => {
        this.highscores = response
      });
  }

  openHowToPlay(content) {
    this.modalService.open(content);
  }
}

interface GuessResponse {
  gameId: string;
  guessesLeft: number;
  visibleWord: string;
  correctGuess: boolean;
  gameState: string;
}

interface ScoreEntry {
  score: number;
  user: string;
}
