The challenge is to build a simple 'hangman' game that works as follows:
1. Chooses a random word out of 6 words: [3dhubs, marvin, print, filament, order, layer]
2. Display the spaces for the letters of the word (eg: '​_ _ _​ _ _' for 'order')
3. The user can choose a letter and if that letter exists in the chosen word it should be shown on the puzzle (eg: asks for 'r' and now it shows '​_ r _​ _ r' for 'order')
4. The user can only ask 5 letters that don't exist in the word and then it's game over
5. If the user wins, congratulate the user and save their high score

Because this is a game we like to see some high scores of participants. It is up to you to define what makes the highest score and how to store them.

Since we have numerous API integrations at 3D Hubs, we do like you to provide a simple API for the Hangman game. Besides being developer friendly we also value our less technical end-users, so please also provide a clean interface for these users to play with.
