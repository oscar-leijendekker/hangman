# Readme

## Hangman test assignment by Oscar Leijendekker

This is a project I built as part of a job application @ 3DHubs. Description of assignment is in assignment.md.

### How to run
Run python index.py from /backend, environment variables `DB_USERNAME`, `DB_PASSWORD` and `DB_NAME` must be set to connect with a postgresql database running on localhost. As this was a test assignment I haven't bothered specifying a url for the DB.

### How to build
* On windows: run package.bat, requires docker. Server will listen on port 5000.
* Other systems: not implemented but that .bat file should tell you all you need.

### Server API
* `/` (GET): returns index.html.
* `/get_highscores` (GET): Returns the highest scores in the game so far, JSON structure: `[{"score": number, "user": string}]`.
* `/path` (GET): returns a static file @ path.
* `/start_game` (POST): Starts the game. Takes empty input. Returns a ResponseData JSON (see below).
* `/guess_letter` (POST): Makes a guess. Takes body `{"gameId": string, "guess": string}`. With gameId the token gotten from start_game and guess the guessed letter. Returns a ResponseData JSON (see below).
* `/save_score` (POST): Adds given name to highscore database (uses score stored for gameId). Takes body `{"gameId": string, "name": string}`. Returns empty string.

ResponseData = `{
      "gameId": string,
      "guessesLeft": number,
      "visibleWord": string,
      "correctGuess": boolean,
      "gameState": string}` with gameState either `"ONGOING"`, `"WON"` or `"LOST"`


### Decisions
* Mimic stack of 3dHubs
* Keep hidden word on server to prevent cheating. Could have hashed the word in a JWT to avoid lookups but this is safer.
* Use uuid4 to identify users, low chance of conflict.
* start_game is a POST request despite not having a body because it cannot be cached.
