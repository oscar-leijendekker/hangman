# consulted sources:
# - https://stackoverflow.com/questions/20646822/how-to-serve-static-files-in-flask
from app import app
import os
from flask import send_from_directory


@app.route('/')
def serve_index():
    print('connected to /')
    return serve_static_file('index.html')

@app.route('/<path:path>', methods=["GET"])
def serve_static_file(path):
    root_dir = os.path.dirname(os.getcwd())
    directory = os.path.join(root_dir, 'frontend/dist')
    return send_from_directory(directory, path)

if __name__ == "__main__":
    app.run()
