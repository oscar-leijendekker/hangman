from app import app
import highscore_db
from datatypes.game import Game
from flask import request

@app.route("/save_score", methods=["POST"])
def save_score():
    json_dict = request.get_json()
    game_id = json_dict["gameId"]
    player_name = json_dict["name"]
    try:
        game = Game.get_by_id(game_id)
    except KeyError:
        return ''

    highscore_db.add_score(name=player_name, score=game.score)
    return ''
