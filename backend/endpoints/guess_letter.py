from app import app
from datatypes.game import Game, GuessResult
from datatypes.client_game_data import ClientGameData, client_game_data_to_json_response
from flask import request

from endpoints.start_game import start_game

@app.route("/guess_letter", methods=["POST"])
def guess_letter():
    json_dict = request.get_json()
    game_id = json_dict["gameId"]
    guessed_letter = json_dict['guess']

    try:
        game = Game.get_by_id(game_id)
    except KeyError:
        return start_game()

    guess_result = game.guess(guessed_letter)
    client_game_data = ClientGameData (
        game_id = game.id,
        guesses_left = game.guesses_left,
        visible_word = game.visible_word(),
        last_guess_correct = guess_result.b_correct,
        game_state = guess_result.game_state
    )

    return client_game_data_to_json_response(client_game_data)
