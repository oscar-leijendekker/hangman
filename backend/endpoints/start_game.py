from app import app
from datatypes.game import Game, GameState
from datatypes.client_game_data import ClientGameData, client_game_data_to_json_response


@app.route("/start_game", methods=["POST"])
def start_game():
    new_game = Game()
    client_game_data = ClientGameData (
        game_id = new_game.id,
        guesses_left = new_game.guesses_left,
        visible_word = new_game.visible_word(),
        last_guess_correct = False,
        game_state = GameState.ONGOING
    )
    return client_game_data_to_json_response(client_game_data)
