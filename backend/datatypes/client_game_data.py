import json
from collections import namedtuple

ClientGameData = namedtuple('ClientGameData', ['game_id', 'guesses_left', 'visible_word', 'last_guess_correct', 'game_state'])

def client_game_data_to_json_response(client_game_data):
    return json.dumps({
        "gameId": client_game_data.game_id,
        "guessesLeft": client_game_data.guesses_left,
        "visibleWord": client_game_data.visible_word,
        "correctGuess": client_game_data.last_guess_correct,
        "gameState": client_game_data.game_state.name
    })
