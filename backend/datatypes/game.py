guessableWords = ["3dhubs", "marvin", "print", "filament", "order", "layer"]
max_guesses = 5
game_max_time_seconds = 15 * 60

import random
from time import time
from uuid import uuid4
from threading import Timer
from functools import partial
from enum import Enum
from collections import namedtuple

class GameState(Enum):
    ONGOING = 0
    WON = 1
    LOST = 2

GuessResult = namedtuple('GuessResult', ['b_correct', 'game_state'])

running_games = {}
class Game:
    def __init__(self):
        self.id = str(uuid4())
        self.start_time = time()
        self.target_word = random.choice(guessableWords)
        self.guesses_left = max_guesses
        self.guessed_letters = set()
        self.score = 0
        running_games[self.id] = self

        #delete game after it times out
        t = Timer(game_max_time_seconds, partial(Game.forget, self))
        t.start()

    @staticmethod
    def get_by_id(id):
        return running_games[id]

    def forget(self):
        del running_games[self.id]

    def visible_word(self):
        return ''.join(l if l in self.guessed_letters else "_" for l in self.target_word)

    def guess(self, letter):
        self.guessed_letters.add(letter)

        if letter in self.target_word:
            letters_left = sum(1 for letter in self.target_word if letter not in self.guessed_letters)

            if letters_left == 0: #game won!
                self.score = 10000 - int((time() - self.start_time)*10) - (max_guesses - self.guesses_left) * 1000
                return GuessResult(b_correct=True, game_state=GameState.WON)
            else:
                return GuessResult(b_correct=True, game_state=GameState.ONGOING)
        else:
            self.guesses_left -= 1
            if self.guesses_left <= 0:
                return GuessResult(b_correct=False, game_state=GameState.LOST)
            else:
                return GuessResult(b_correct=False, game_state=GameState.ONGOING)
